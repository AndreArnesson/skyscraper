import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import secrets


def send_email(login_addr, to_addrs, subject, html_content):
    message = MIMEText(html_content, 'html')

    message['From'] = 'Pontus'
    message['Subject'] = subject

    msg_full = message.as_string()
    
    # setup the email server,
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    
    # add account login name and password,
    server.login(login_addr, secrets.LOGIN_PASS)
    
    # send the email
    server.sendmail(login_addr, to_addrs, msg_full)
    
    # disconnect from the server
    server.quit()


def send_email_exception(message, exception):
    # set the 'from' address,
    loginaddr = "tstning@gmail.com"
    
    # Add recipients
    to_addrs = ["andre.arnesson@gmail.com"]

    subject = "Crash"
    msg_content = f'''
      <div> 
        <h1>{message}</h1>
        <h2>{exception}</h2>

      <div>
    '''

    send_email(loginaddr, to_addrs, subject, msg_content)

def send_flight_email(flight_info, url, from_airport, to_airport, day):
    # set the 'from' address,
    loginaddr = "tstning@gmail.com"
    
    # Add recipients
    # to_addrs = [loginaddr, "lennart.arnesson1@gmail.com", "andre.arnesson@gmail.com"]
    to_addrs = ["andre.arnesson@gmail.com"]
    #to_addrs = [loginaddr]

    price = flight_info["price"]
    time = flight_info["time"]
    number_stops = flight_info["number_stops"]
    subject = f"Flight to {to_airport}"
    msg_content = f'''
      <div> 
        <h1>Flyget kostar {price}!</h3>
        <h2>Antal Timmar: {time} --- den {day}e juli</h1>
        <h3>Så här många stop är det {number_stops}</h5>
        <h3>Från flygplats {from_airport}</h6>
        <p>Här har du URL: <a href="{url}">Klicka här!!</a></p>
      <div>
    '''

    send_email(loginaddr, to_addrs, subject, msg_content)

def send_csv_flight(file):
    # set the 'from' address,
    loginaddr = "tstning@gmail.com"
    
    # Add recipients
    # to_addrs = [loginaddr, "lennart.arnesson1@gmail.com", "andre.arnesson@gmail.com"]
    to_addrs = ["andre.arnesson@gmail.com"]
    #to_addrs = [loginaddr]
    subject = "Complete csv flight info"

    msg = MIMEMultipart()
    msg['From'] = loginaddr
    msg['To'] = ', '.join(to_addrs)
    msg['Subject'] = subject

    with open(file, 'rb') as file:
        csv_part = MIMEApplication(file.read(), Name=file.name)
        csv_part['Content-Disposition'] = f'attachment; filename=flights.csv'
        msg.attach(csv_part)

    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.login(loginaddr, secrets.LOGIN_PASS)
        smtp.send_message(msg)
