from time import sleep, strftime
from random import randint
import time
import csv
import os.path
from selenium import webdriver
import re


from selenium.webdriver.chrome.service import Service
from selenium import webdriver
from selenium.webdriver.common.by import By
from skyscraper_mail import send_flight_email, send_email_exception, send_csv_flight
import random

flightfile = "flightsspain.csv"


def clear_popup():
    popup_buttons = '//button[@class = "Iqt3 Iqt3-mod-stretch Iqt3-mod-bold Button-No-Standard-Style Iqt3-mod-variant-outline Iqt3-mod-theme-base Iqt3-mod-shape-rounded-small Iqt3-mod-shape-mod-default Iqt3-mod-spacing-default Iqt3-mod-size-small"]'
    driver.find_elements(By.XPATH, popup_buttons)[0].click()
    time.sleep(16)


def find_top_flights(website):
    driver.get(website)
    time.sleep(16)


    flight_element = get_first_flight()
    time.sleep(16)
    cheapest_flight = get_flight_info(flight_element)

    time.sleep(16)

    #Sort by best instead of cheapest
    best_flight = find_best_flight()

    #Compare cheapest and best
    print("Cheapest Flight: ", cheapest_flight)
    print("Best flight: ", best_flight)

    time.sleep(16)
    return (cheapest_flight, best_flight)
    
def find_best_flight():
    try:
        test = driver.find_elements(By.CLASS_NAME, "Hv20-option")[1].click()
    except Exception as e:
         driver.maximize_window()
         driver.implicitly_wait(20)
         send_email_exception("krashar vid när klicka på best flight", e)
         test = driver.find_elements(By.CLASS_NAME, "Hv20-option")[1].click()
    time.sleep(16)
    flight_element = get_first_flight()
    time.sleep(12)
    flight_info = get_flight_info(flight_element)
    return flight_info


def get_first_flight():
    try:
        first_flight = driver.find_element(By.CLASS_NAME, "Ui-Flights-Results-Components-ListView-container").find_elements(By.CLASS_NAME, "nrc6")[0]
    except:
        print("krashar egentligen")
        driver.maximize_window()
        driver.implicitly_wait(20)
        first_flight = driver.find_element(By.CLASS_NAME, "Ui-Flights-Results-Components-ListView-container").find_elements(By.CLASS_NAME, "nrc6")[0]
    #If it is an ad
    try:
        is_ad = len(first_flight.find_elements(By.CLASS_NAME, "J0g6-fsr-text")) != 0
        if(is_ad): 
            return driver.find_element(By.CLASS_NAME, "Ui-Flights-Results-Components-ListView-container").find_elements(By.CLASS_NAME, "nrc6")[1]
        else:
            return first_flight
    except Exception as e:
         send_email_exception("Krashade när kollade efter ad", e)
         driver.maximize_window()
         driver.implicitly_wait(20)
         is_ad = len(first_flight.find_elements(By.CLASS_NAME, "J0g6-fsr-text")) != 0
         if(is_ad): 
            return driver.find_element(By.CLASS_NAME, "Ui-Flights-Results-Components-ListView-container").find_elements(By.CLASS_NAME, "nrc6")[1]
         else:
            return first_flight


def get_flight_info(flight_element):
    time.sleep(16)
    try:
        price = flight_element.find_element(By.CLASS_NAME, "f8F1-price-text").text
        time.sleep(15)
        info_boxes = flight_element.find_elements(By.CLASS_NAME, "vmXl.vmXl-mod-variant-default")
    except Exception as e:
        send_email_exception("Borde egentligen krashat vid get_flight_info", e)
        driver.maximize_window()
        driver.implicitly_wait(20)
        flight_element = get_first_flight()
        time.sleep(14)
        price = flight_element.find_element(By.CLASS_NAME, "f8F1-price-text").text
        time.sleep(14)
        info_boxes = flight_element.find_elements(By.CLASS_NAME, "vmXl.vmXl-mod-variant-default")
    number_stops = info_boxes[0].text
    time.sleep(16)
    hours = info_boxes[1].text

    flight_info = {
        "number_stops": number_stops,
        "time": hours,
        "price": price
    }

    return flight_info

def search_flight(from_day, to_day, from_airport, to_airport, base_url, cheapest_price_limit, best_price_limit):
    
    for day in range(from_day, to_day+1):
        if(day<10):
            day= "0"+ str(day)
        website = base_url.format(day = day, from_airport = from_airport, to_airport = to_airport)
        cheapeast_flight, best_flight = find_top_flights(website)
        time.sleep(16)
        cheap_price = int(cheapeast_flight["price"].replace(" ", "").replace("kr", ""))
        best_price = int(best_flight["price"].replace(" ", "").replace("kr", ""))

        hours = int(re.search(r'(\d{1,2})t', cheapeast_flight["time"]).group(1))
        if(cheap_price < cheapest_price_limit and hours<12):
                    send_flight_email(cheapeast_flight, website, from_airport, to_airport, day)
        elif(best_price < best_price_limit):
                    send_flight_email(best_flight, website, from_airport, to_airport, day)
        
        if(hours>12):
            write_to_SCV(website, best_price, day, best_flight["number_stops"], best_flight["time"])
        else:
            write_to_SCV(website, cheap_price, day, cheapeast_flight["number_stops"], cheapeast_flight["time"]) 


def write_to_SCV(website, price , day, number_stops, time):
        if flight_exists(website):
            delete_and_insert_row(website, price, day, number_stops, time)
        else:
            with open(flightfile, mode='a', newline='') as file:
                print("fanns inte så skriver ny rad")
                writer = csv.writer(file)
                writer.writerow([website, str(day)+ " july", str(price) + " kr", str(number_stops), str(time) + " timmar"])
                

def flight_exists(website):
     with open(flightfile, mode='r') as file:
        reader = csv.reader(file)
        for row in reader:
            if len(row) == 0:
                 return False
            if row[0] == website:
                return True
        return False
     
def delete_and_insert_row(website, price, day, number_stops, hour):
    rows = []
    with open(flightfile, mode='r') as file:
        reader = csv.reader(file)
        for row in reader:
            if len(row) == 0:
                 print("tom rad")
            elif row[0] != website:
                rows.append(row)
    #ta bort filen och gör om den
    time.sleep(3)
    if(os.path.exists(flightfile) and os.path.isfile(flightfile)):
        os.remove(flightfile)
        print("deleting old file")
        time.sleep(3)
    else:
         print("finns inte tydligen")
    
    with open(flightfile, mode='w', newline='') as file:
        writer = csv.writer(file)
        for row in rows:
            writer.writerow(row)
        writer.writerow([website, str(day)+ " july", str(price) + " kr", str(number_stops), str(hour) + " timmar"])
     

def sort_csv_by_price():
    # Read the CSV file into a list of dictionaries
    with open(flightfile, mode='r') as file:
        reader = csv.DictReader(file)
        data = [row for row in reader]
    
    # Sort the data based on the 'price' column
    data.sort(key=lambda row: float(row['price'].replace('kr', '').strip()))
    
    # Write the sorted data back to the CSV file
    with open(flightfile, mode='w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=data[0].keys())
        writer.writeheader()
        writer.writerows(data)

if __name__ == '__main__':
    chromedriver_path = "C:/Users/Andre/Documents/Projects/chromedriver_win32/chromedriver.exe"
    service = Service(chromedriver_path)
    options = webdriver.ChromeOptions()
    driver = webdriver.Chrome(service=service, options=options)

    departure_flights_inputs={'Departure': " CPH",
                            'Arrival': " SCL",
                            'Date': "Jul 15, 2023"}
    driver.get("https://www.momondo.se")
    time.sleep(16)
    clear_popup()
    from_airport = ""

    #create csv file
    if not os.path.isfile(flightfile):
         with open(flightfile, mode='a', newline='') as file:
             writer = csv.writer(file)
             writer.writerow(['website', 'day', 'price', 'number_stops', 'time'])  # Write header row

    while(True):
        try:
            search_list = [
                 (25,28, "CPH", "AGP", "https://www.momondo.se/flight-search/{from_airport}-{to_airport}/2023-06-{day}?sort=price_a", 1000, 1500),  #Check from CPH
                 (25,28, "GOT", "AGP", "https://www.momondo.se/flight-search/{from_airport}-{to_airport}/2023-06-{day}?sort=price_a", 1000, 1500), #Check from Landvetter
                 (25,28, "STO", "AGP", "https://www.momondo.se/flight-search/{from_airport}-{to_airport}/2023-06-{day}?sort=price_a", 700, 1100), #Check from Stockholm
                 (1,2, "AGP", "STO", "https://www.momondo.se/flight-search/{from_airport}-{to_airport}/2023-07-{day}?sort=price_a", 700, 1000), #
                 (1,2, "AGP", "CPH", "https://www.momondo.se/flight-search/{from_airport}-{to_airport}/2023-07-{day}?sort=price_a", 1000, 1500), 
                 (1,2, "AGP", "GOT", "https://www.momondo.se/flight-search/{from_airport}-{to_airport}/2023-07-{day}?sort=price_a", 1000, 1500) #C
            ]
            random.shuffle(search_list)
            
            for search in search_list:
                 search_flight(*search)
            sort_csv_by_price()
            send_csv_flight(flightfile)

                

        except Exception as e: 
            print("Krashar under while loopen. Kör om hela programmet")
            send_email_exception("Hela programmet krashar. I while(true)", e)


       

